
package com.matt.virtlpt

import java.util.Random

import android.content.SharedPreferences

public class VirtlPt {
    private val HEALTH_PREF = "health"
    private val FED_PREF = "fed"
    private val POOPED_PREF = "pooped"

    private val POOP_RATE = 240
    private val HUNGER_LIMIT = 50
    private val HUNGER_RATE = 1
    private val HUNGER_HEALTH_HIT = 10
    private val POOPED_HEALTH_HIT = 1
    private val FEED_FED_BOOST = 100
    private val OVEREAT_HEALTH_HIT = 200
    private val WELL_FED_LIMIT = 300
    private val WELL_FED_HEALTH_BOOST = 1
    private val HUNGER_PLAY_HEALTH_HIT = 200
    private val PLAY_HEALTH_BOOST = 50
    private val PLAY_HUNGER_HIT = 40
    private val ILL_HEALTH = 250

    val MAX_HEALTH = 1000
    val MAX_FED = 600

    var health = MAX_HEALTH
        private set
    var fed = MAX_FED
        private set
    var pooped = false
        private set
    val isIll : Boolean
        get () = health < ILL_HEALTH
    val isDead : Boolean
        get () = health <= 0
    val isHungry : Boolean
        get () = fed < HUNGER_LIMIT

    private val random = Random()

    /**
     * Returns true if pet wants to alert owner
     */
    fun tick() : Boolean {
        val prevPooped = pooped
        val prevHungry = isHungry

        if (fed > 0)
            addFed(-HUNGER_RATE)

        if (isHungry)
            addHealth(-HUNGER_HEALTH_HIT)
        else if (fed > WELL_FED_LIMIT)
            addHealth(WELL_FED_HEALTH_BOOST)

        if (pooped)
            addHealth(-POOPED_HEALTH_HIT)

        if (random.nextInt(POOP_RATE) == 0)
            pooped = true

        return ((pooped && !prevPooped) ||
                (isHungry && !prevHungry))
    }

    fun feed() {
        val nextFed = fed + FEED_FED_BOOST

        if (nextFed > MAX_FED) {
            addHealth(-OVEREAT_HEALTH_HIT)
            fed = MAX_FED
            pooped = true
        } else {
            fed = nextFed
        }
    }

    fun play() {
        // won't play if ill
        if (isIll)
            return

        if (isHungry)
            addHealth(-HUNGER_PLAY_HEALTH_HIT)
        else
            addHealth(PLAY_HEALTH_BOOST)
    }

    fun clean() {
        pooped = false
    }

    fun readFromSharedPrefs(prefs : SharedPreferences) {
        health = prefs.getInt(HEALTH_PREF, MAX_HEALTH)
        fed = prefs.getInt(FED_PREF, MAX_FED)
        pooped = prefs.getBoolean(POOPED_PREF, false)
    }

    fun writeToSharedPrefs(prefs : SharedPreferences) {
        val editor = prefs.edit()
        editor.putInt(HEALTH_PREF, health)
        editor.putInt(FED_PREF, fed)
        editor.putBoolean(POOPED_PREF, pooped)
        editor.apply();
    }

    private fun addHealth(delta : Int) {
        health += delta
        if (health < 0) health = 0
        if (health > MAX_HEALTH) health = MAX_HEALTH
    }

    private fun addFed(delta : Int) {
        fed += delta
        if (fed < 0) fed = 0
        if (fed > MAX_FED) fed = MAX_FED
    }
}
