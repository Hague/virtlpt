
package com.matt.virtlpt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

public class StartAppReceiver : BroadcastReceiver() {

    override public fun onReceive(context : Context,
                                  intext : Intent) {
        val i = Intent(context, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

}
