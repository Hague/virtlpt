
package com.matt.virtlpt

import java.util.Random
import java.util.Timer
import java.util.TimerTask

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import kotlinx.coroutines.launch

public class MainActivity : AppCompatActivity() {

    private val CENTER_UP = "com.wahoofitness.bolt.buttons.center_button.up"
    private val LEFT_UP = "com.wahoofitness.bolt.buttons.left_button.up"
    private val RIGHT_UP = "com.wahoofitness.bolt.buttons.right_button.up"
    private val POWER_UP = "com.wahoofitness.bolt.buttons.power_button.up"

    private val PATTERN_FIELD = "pattern"

    private val BUZZ_PATTERN = "com.wahoofitness.bolt.service.BBuzzerManager.PATTERN"
    private val BUZZ_ALERT = "B#1046 S#50 B#0 S#1 B#1318 S#50 B#0 S#1 B#1567 S#50 B#0 S#1 B#2093 S#80 B#0 S#1"
    private val BUZZ_WIN = "B#1479 S#50 B#0 S#5 B#1567 S#50 B#0 S#5 B#1244 S#50 B#0 S#5 B#1318 S#100 B#0 REPEAT#3"

    private val LED_PATTERN = "com.wahoofitness.bolt.service.BLedManager.PATTERN"
    private val LED_CLEAR = "com.wahoofitness.bolt.service.BLedManager.CLRALL"

    private val LED_WASH = "T0#0000ff T1#0 T2#0000ff T3#0 T4#0000ff T5#0 T6#0000ff T7#0 S#1000 T0#0 T1#0000ff T2#0 T3#0000ff T4#0 T5#0000ff T6#0 S#1000 REPEAT"
    private val LED_PLAY = "T3#00ff00 S#250 T3#0 T2#00ff00 S#250 T2#0 T1#00ff00 S#250 T1#0 T0#00ff00 S#250 T0#0 T3#00ff00 S#250 T3#0 T4#00ff00 S#250 T4#0 T5#00ff00 S#250 T5#0 T6#00ff00 S#250 T6#0 REPEAT"
    private val LED_WIN = "T0#0000ff T1#ff0000 T2#00ff00 T3#0000ff T4#ff0000 T5#00ff00 T6#ff0000 S#500 T0#ff0000 T1#00ff00 T2#0000ff T3#ff0000 T4#00ff00 T5#ff0000 T6#0000ff S#500 REPEAT"

    private val REFRESH_TIME = 1

    private var timer : Timer? = null

    private var healthView : TextView? = null
    private var fedView : TextView? = null
    private var petView : ImageView? = null
    private var leftView : TextView? = null
    private var centerView : TextView? = null
    private var rightView : TextView? = null

    private var numFrames = 0
    private var virtlpt = VirtlPt()
    private val random = Random()

    val br : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            when (intent.action) {
                POWER_UP -> onPowerUp()
                LEFT_UP -> state.onLeft()
                CENTER_UP -> state.onCenter()
                RIGHT_UP -> state.onRight()
            }
            draw()
        }
    }

    override protected fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        petView = findViewById(R.id.pet) as ImageView
        healthView = findViewById(R.id.health) as TextView
        fedView = findViewById(R.id.fed) as TextView
        leftView = findViewById(R.id.left_button) as TextView
        centerView = findViewById(R.id.center_button) as TextView
        rightView = findViewById(R.id.right_button) as TextView
    }

    override protected fun onResume() {
        super.onResume()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        virtlpt.readFromSharedPrefs(prefs)
        makeReceiver()
        startAutomaticRefresh()
    }

    override protected fun onPause() {
        super.onPause()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        virtlpt.writeToSharedPrefs(prefs)
        unregisterReceiver(br)
        stopAutomaticRefresh()
    }

    private fun makeReceiver() {
        val filter = IntentFilter()
        filter.addAction(CENTER_UP)
        filter.addAction(LEFT_UP)
        filter.addAction(RIGHT_UP)
        filter.addAction(POWER_UP)

        registerReceiver(br, filter)
    }

    private fun onPowerUp() {
        finishAndRemoveTask()
    }

    private fun startAutomaticRefresh() {
        val rate : Long = 1000L * REFRESH_TIME

        timer?.cancel()
        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                lifecycleScope.launch { tick() }
            }
        }, 0, rate);
    }

    private fun stopAutomaticRefresh() {
        timer?.cancel()
    }

    private fun tick() {
        numFrames += 1
        update()
        draw()
    }

    private fun draw() {
        val petImageId = state.getImageId()
        petView?.setImageResource(petImageId)

        setButton(leftView, state.getLeftTextId())
        setButton(centerView, state.getCenterTextId())
        setButton(rightView, state.getRightTextId())

        setHealthBar()
        setFedBar()
    }

    private fun setHealthBar() {
        val hearts : Int = 5 * virtlpt.health / virtlpt.MAX_HEALTH
        when (hearts) {
            0 -> healthView?.setText("")
            1 -> healthView?.setText(getString(R.string.health1))
            2 -> healthView?.setText(getString(R.string.health2))
            3 -> healthView?.setText(getString(R.string.health3))
            else -> healthView?.setText(getString(R.string.health4))
        }
    }

    private fun setFedBar() {
        val fed : Int = 5 * virtlpt.fed / virtlpt.MAX_FED
        when (fed) {
            0 -> fedView?.setText("")
            1 -> fedView?.setText(getString(R.string.fed1))
            2 -> fedView?.setText(getString(R.string.fed2))
            3 -> fedView?.setText(getString(R.string.fed3))
            else -> fedView?.setText(getString(R.string.fed4))
        }
    }

    private fun update() {
        if (virtlpt.tick())
            sendPatternIntent(BUZZ_PATTERN, BUZZ_ALERT)

        if (virtlpt.isDead)
            setState(deadState)
        else
            state.onTick()
    }

    private fun setButton(view : TextView?, stringId : Int) {
        val text = if (stringId > -1) getString(stringId) else ""
        view?.setText(text)
    }

    private fun setNormalState() {
        state.onEnd()
        state = normalState
        state.onStart()
    }

    private fun setState(newState : State) {
        state.onEnd()
        state = newState
        state.onStart()
    }

    private fun sendPatternIntent(action : String,
                                  pattern : String? = null) {
        Intent().also { intent ->
            intent.setAction(action)
            if (pattern != null)
                intent.putExtra(PATTERN_FIELD, pattern)
            sendBroadcast(intent)
        }
    }

    private interface State {
        fun onStart() { }
        fun onEnd() { }

        fun onTick() { }
        fun onLeft() { }
        fun onCenter() { }
        fun onRight()  { }

        fun getImageId() : Int { return -1 }
        fun getLeftTextId() : Int { return -1 }
        fun getCenterTextId() : Int { return -1 }
        fun getRightTextId() : Int { return -1 }
    }

    private val normalState = object : State {
        override fun getImageId() : Int {
            if (virtlpt.isIll) {
                return if (virtlpt.pooped) R.drawable.unwell_poop
                       else R.drawable.unwell
            } else {
                if (numFrames % 6 == 5) {
                    return if (virtlpt.pooped) R.drawable.normal_blink_poop
                           else R.drawable.normal_blink
                } else {
                    return if (virtlpt.pooped) R.drawable.normal_poop
                           else R.drawable.normal
                }
            }
        }

        override fun onLeft() {
            if (!virtlpt.isIll) {
                virtlpt.play()
                setState(playState)
            }
        }

        override fun onCenter() {
            virtlpt.clean()
            setState(cleanState)
        }

        override fun onRight() {
            virtlpt.feed()
            setState(feedState)
        }

        override fun getLeftTextId() : Int {
            return if (!virtlpt.isIll) R.string.play
                   else -1
        }
        override fun getCenterTextId() : Int { return R.string.clean }
        override fun getRightTextId() : Int { return R.string.feed }
    }

    private val playState = object : State {
        private val PLAY_LEN = 10
        private var playTime = 0
        private var playerLeft = true

        override fun onStart() {
            playTime = 0
            playerLeft = true
            sendPatternIntent(LED_PATTERN, LED_PLAY)
        }

        override fun onEnd() {
            sendPatternIntent(LED_CLEAR)
        }

        override fun onTick() {
            playTime += 1
            if (playTime > PLAY_LEN) {
                val petLeft = random.nextBoolean()
                setState(PlayEndState(playerLeft, petLeft))
            }
        }

        override fun onLeft() { playerLeft = true }
        override fun onRight()  { playerLeft = false }

        override fun getImageId() : Int {
            return if (numFrames % 2 == 0) R.drawable.wing_right
                   else R.drawable.wing_left
        }

        override fun getLeftTextId() : Int {
            return if (playerLeft) R.string.left_caps
                   else R.string.left_small
        }

        override fun getRightTextId() : Int {
            return if (playerLeft) R.string.right_small
                   else R.string.right_caps
        }
    }

    private inner class PlayEndState(val playerLeft : Boolean,
                                     val petLeft : Boolean)
            : State {
        private val PLAY_LEN = 2
        private var playTime = 0

        override fun onStart() {
            playTime = 0
        }

        override fun onTick() {
            playTime += 1
            if (playTime > PLAY_LEN) {
                if (playerLeft == petLeft) {
                    sendPatternIntent(BUZZ_PATTERN, BUZZ_WIN)
                    setState(winState)
                } else {
                    setNormalState()
                }
            }
        }

        override fun getImageId() : Int {
            return if (petLeft) R.drawable.wing_left
                   else R.drawable.wing_right
        }

        override fun getLeftTextId() : Int {
            return if (playerLeft) R.string.left_caps
                   else -1
        }

        override fun getRightTextId() : Int {
            return if (!playerLeft) R.string.right_caps
                   else -1
        }
    }

    private inner class AnimationState(val frame1id : Int,
                                       val frame2id : Int,
                                       val ledPattern : String? = null)
            : State {

        private val PLAY_LEN = 5
        private var playTime = 0

        override fun onStart() {
            playTime = 0
            if (ledPattern != null)
                sendPatternIntent(LED_PATTERN, ledPattern)
        }

        override fun onEnd() {
            if (ledPattern != null)
                sendPatternIntent(LED_CLEAR)
        }

        override fun onTick() {
            playTime += 1
            if (playTime > PLAY_LEN) {
                setNormalState()
            }
        }

        override fun getImageId() : Int {
            return if (numFrames % 2 == 0) frame1id
                   else frame2id
        }
    }

    private val feedState = AnimationState(R.drawable.eat1,
                                           R.drawable.eat2)

    private val cleanState = AnimationState(R.drawable.wash1,
                                            R.drawable.wash2,
                                            LED_WASH)

    private val winState = AnimationState(R.drawable.flap1,
                                          R.drawable.flap2,
                                          LED_WIN)

    private val deadState = object : State {
        override fun getImageId() : Int {
            return R.drawable.dead
        }

        override fun getCenterTextId() : Int {
            return R.string.new_pet
        }

        override fun onCenter() {
            virtlpt = VirtlPt()
            setNormalState()
        }
    }

    private var state : State = normalState
}
