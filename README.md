
# VIRTLPT - Virtual Pet for the Wahoo ELEMNT Bolt

Since the ELEMNT Bolt is basically a souped up Tamagotchi, here's a virtual pet
for it.

![Owl](app/src/main/res/drawable/normal.png)

## Compilation

Use Gradle

    $ ./gradlew build

## Installation

First connect the Bolt to your PC via USB. Then [unlock ADB][joshua]
on the ELEMNT Bolt by pressing "power" and "up" simultaneously (a few
times to be sure).

Then run

    $ ./gradlew installDebug

or sign the APK produced by `./gradlew build` and install it as usual.

You then need to start the game manually, but only once

    $ adb shell am start -n com.matt.virtlpt/com.matt.virtlpt.MainActivity

This is a security feature [since Android 3.1][receiving]. Afterwards,
double clicking the power button will work.

## Playing

Double click the power button to start the game. Single click power to
close (and save) the game.

## Uninstall

To uninstall, with ADB unlocked as above, run

    $ adb uninstall com.matt.virtlpt

## Screenshots

![Normal mode](screenshots/normal.png)
![Playing](screenshots/play.png)
![Washing](screenshots/wash.png)
![Feeding](screenshots/feed.png)
![Unwell](screenshots/unwell.png)

[joshua]: https://joshua0.dreamwidth.org/66991.html
[receiving]: https://commonsware.com/blog/2011/07/13/boot-completed-regression-confirmed.html
